//
//  ViewController.swift
//  SignInThatWorks
//
//  Created by Chase Conaway on 4/29/19.
//  Copyright © 2019 Chase Conaway. All rights reserved.
//

import GoogleAPIClientForREST
import GoogleSignIn
import iOSDropDown
import UIKit

class ViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {
    
    // If modifying these scopes, delete your previously saved credentials by
    
    // resetting the iOS simulator or uninstall the app.
    
    private let scopes = [kGTLRAuthScopeSheetsSpreadsheets]
    private let service = GTLRSheetsService()
    
    let signInButton = GIDSignInButton()
    
    let output = UITextView()
    let cell = UITextView()
    let sheetTitle = UITextView()
    let sheetID = UITextView()
    let warning = UITextView()
    var arrayOfSheets: Array<String> = []
    let alert = UIAlertController(title: "There is no chosen match or team number!", message: "Make sure to choose the team number or match number!", preferredStyle: .alert)
    let level1Hatches = UITextView()
    let level2Hatches = UITextView()
    let level3Hatches = UITextView()
    let level1Cargo = UITextView()
    let level2Cargo = UITextView()
    let level3Cargo = UITextView()
    let level1Climb = UITextView()
    let level2Climb = UITextView()
    let level3Climb = UITextView()
    let matchNumber = UITextView()
    
    let spreadsheetId = globalSpreadsheetID
    
    var count = 1
    var idGenerator = 0
    var numberOfSheets = 0
    var sheet = 1
    var array: Array<Int> = []
    var matchesArray: Array<String> = []
    var lvl1H = 0
    var lvl2H = 0
    var lvl3H = 0
    var lvl1C = 0
    var lvl2C = 0
    var lvl3C = 0
    var lvl1Cl = 0
    var lvl2Cl = 0
    var lvl3Cl = 0
    var averageDivisor = ""
    
    @IBOutlet weak var teamDropDown: DropDown!
    @IBOutlet weak var matchDropDown: DropDown!
    @IBOutlet weak var hatchesButton: UIButton!
    @IBOutlet weak var hatches2Button: UIButton!
    @IBOutlet weak var hatches3Button: UIButton!
    @IBOutlet weak var cargo1Button: UIButton!
    @IBOutlet weak var cargo2Button: UIButton!
    @IBOutlet weak var cargi3Button: UIButton!
    @IBOutlet weak var climb1Button: UIButton!
    @IBOutlet weak var climb2Button: UIButton!
    @IBOutlet weak var climb3Button: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        //GIDSignIn.sharedInstance()?.signOut()
        
        // Configure Google Sign-in.
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = scopes
        GIDSignIn.sharedInstance().signInSilently()
        
        // Add the sign-in button.
        
        signInButton.center = CGPoint(x: 100, y: 100)
        
        view.addSubview(signInButton)
        
        // Add a UITextView to display output.
        output.frame = CGRect(x: 0, y: 0, width: 100, height: 150)
        output.isEditable = false
        output.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        output.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        output.center = CGPoint(x: 300, y: 100)
        output.isHidden = false
        output.backgroundColor = UIColor(red: 127/255.0, green: 128/255.0, blue: 131/255.0, alpha: 1.0)
        
        level1Hatches.frame = CGRect(x: 0, y: 0, width: 75, height: 22)
        level1Hatches.isEditable = false
        level1Hatches.textAlignment = .center
        level1Hatches.contentInset = UIEdgeInsets(top: -5, left: 0, bottom: 0, right: 0)
        level1Hatches.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        level1Hatches.isHidden = false
        level1Hatches.center = CGPoint(x: 360, y: 210)
        level1Hatches.font = UIFont(name: "System", size: 18)
        level1Hatches.textColor = UIColor.white
        level1Hatches.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        
        level2Hatches.frame = CGRect(x: 0, y: 0, width: 75, height: 22)
        level2Hatches.isEditable = false
        level2Hatches.textAlignment = .center
        level2Hatches.contentInset = UIEdgeInsets(top: -5, left: 0, bottom: 0, right: 0)
        level2Hatches.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        level2Hatches.isHidden = false
        level2Hatches.center = CGPoint(x: 360, y: 242)
        level2Hatches.font = UIFont(name: "System", size: 18)
        level2Hatches.textColor = UIColor.white
        level2Hatches.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        
        level3Hatches.frame = CGRect(x: 0, y: 0, width: 75, height: 22)
        level3Hatches.isEditable = false
        level3Hatches.textAlignment = .center
        level3Hatches.contentInset = UIEdgeInsets(top: -5, left: 0, bottom: 0, right: 0)
        level3Hatches.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        level3Hatches.isHidden = false
        level3Hatches.center = CGPoint(x: 360, y: 272)
        level3Hatches.font = UIFont(name: "System", size: 18)
        level3Hatches.textColor = UIColor.white
        level3Hatches.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        
        level1Cargo.frame = CGRect(x: 0, y: 0, width: 75, height: 22)
        level1Cargo.isEditable = false
        level1Cargo.textAlignment = .center
        level1Cargo.contentInset = UIEdgeInsets(top: -5, left: 0, bottom: 0, right: 0)
        level1Cargo.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        level1Cargo.isHidden = false
        level1Cargo.center = CGPoint(x: 360, y: 348)
        level1Cargo.font = UIFont(name: "System", size: 18)
        level1Cargo.textColor = UIColor.white
        level1Cargo.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        
        level2Cargo.frame = CGRect(x: 0, y: 0, width: 75, height: 22)
        level2Cargo.isEditable = false
        level2Cargo.textAlignment = .center
        level2Cargo.contentInset = UIEdgeInsets(top: -5, left: 0, bottom: 0, right: 0)
        level2Cargo.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        level2Cargo.isHidden = false
        level2Cargo.center = CGPoint(x: 360, y: 380)
        level2Cargo.font = UIFont(name: "System", size: 18)
        level2Cargo.textColor = UIColor.white
        level2Cargo.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        
        level3Cargo.frame = CGRect(x: 0, y: 0, width: 75, height: 22)
        level3Cargo.isEditable = false
        level3Cargo.textAlignment = .center
        level3Cargo.contentInset = UIEdgeInsets(top: -5, left: 0, bottom: 0, right: 0)
        level3Cargo.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        level3Cargo.isHidden = false
        level3Cargo.center = CGPoint(x: 360, y: 412)
        level3Cargo.font = UIFont(name: "System", size: 18)
        level3Cargo.textColor = UIColor.white
        level3Cargo.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)

        level1Climb.frame = CGRect(x: 0, y: 0, width: 75, height: 22)
        level1Climb.isEditable = false
        level1Climb.textAlignment = .center
        level1Climb.contentInset = UIEdgeInsets(top: -5, left: 0, bottom: 0, right: 0)
        level1Climb.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        level1Climb.isHidden = false
        level1Climb.center = CGPoint(x: 360, y: 496)
        level1Climb.font = UIFont(name: "System", size: 18)
        level1Climb.textColor = UIColor.white
        level1Climb.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)

        level2Climb.frame = CGRect(x: 0, y: 0, width: 75, height: 22)
        level2Climb.isEditable = false
        level2Climb.textAlignment = .center
        level2Climb.contentInset = UIEdgeInsets(top: -5, left: 0, bottom: 0, right: 0)
        level2Climb.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        level2Climb.isHidden = false
        level2Climb.center = CGPoint(x: 360, y: 528)
        level2Climb.font = UIFont(name: "System", size: 18)
        level2Climb.textColor = UIColor.white
        level2Climb.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)

        level3Climb.frame = CGRect(x: 0, y: 0, width: 75, height: 22)
        level3Climb.isEditable = false
        level3Climb.textAlignment = .center
        level3Climb.contentInset = UIEdgeInsets(top: -5, left: 0, bottom: 0, right: 0)
        level3Climb.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        level3Climb.isHidden = false
        level3Climb.center = CGPoint(x: 360, y: 560)
        level3Climb.font = UIFont(name: "System", size: 18)
        level3Climb.textColor = UIColor.white
        level3Climb.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        
        cell.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        cell.isEditable = true
        cell.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        cell.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        cell.isHidden = false
        cell.center = CGPoint(x: 200, y: 300)
        cell.textColor = UIColor.white
        cell.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        
        sheetTitle.frame = CGRect(x: 0, y: 0, width: 75, height: 22)
        sheetTitle.isEditable = true
        sheetTitle.contentInset = UIEdgeInsets(top: -10, left: 0, bottom: 0, right: 0)
        sheetTitle.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        sheetTitle.isHidden = false
        sheetTitle.center = CGPoint(x: 360, y: 50)
        sheetTitle.textColor = UIColor.white
        sheetTitle.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        
        sheetID.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        sheetID.isEditable = true
        sheetID.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        sheetID.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        sheetID.isHidden = true
        sheetID.center = CGPoint(x: 100, y: 200)
        sheetID.textColor = UIColor.white
        sheetID.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        
        warning.frame = CGRect(x: 0, y: 0, width: 100, height: 25)
        warning.isEditable = false
        warning.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        warning.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        warning.isHidden = true
        warning.center = CGPoint(x: 100, y: 160)
        warning.text = "Numbers only!!"
        warning.textColor = UIColor.white
        warning.backgroundColor = UIColor.black
        
        //view.addSubview(output)
        view.addSubview(level1Hatches)
        view.addSubview(level2Hatches)
        view.addSubview(level3Hatches)
        view.addSubview(level1Cargo)
        view.addSubview(level2Cargo)
        view.addSubview(level3Cargo)
        view.addSubview(level1Climb)
        view.addSubview(level2Climb)
        view.addSubview(level3Climb)
        //view.addSubview(cell)
        view.addSubview(sheetTitle)
        //view.addSubview(sheetID)
        //view.addSubview(warning)
        
    }
    
    override func viewWillLayoutSubviews() {
        level1Hatches.translatesAutoresizingMaskIntoConstraints = false
        level1Hatches.topAnchor.constraint(equalTo: hatchesButton.topAnchor, constant: 6).isActive = true
        level1Hatches.bottomAnchor.constraint(equalTo: hatchesButton.bottomAnchor, constant: -6).isActive = true
        level1Hatches.leadingAnchor.constraint(equalTo: hatchesButton.leadingAnchor, constant: 306).isActive = true
        level1Hatches.trailingAnchor.constraint(equalTo: hatchesButton.trailingAnchor, constant: 308).isActive = true
        
        level2Hatches.translatesAutoresizingMaskIntoConstraints = false
        level2Hatches.topAnchor.constraint(equalTo: hatches2Button.topAnchor, constant: 6).isActive = true
        level2Hatches.bottomAnchor.constraint(equalTo: hatches2Button.bottomAnchor, constant: -6).isActive = true
        level2Hatches.leadingAnchor.constraint(equalTo: hatches2Button.leadingAnchor, constant: 306).isActive = true
        level2Hatches.trailingAnchor.constraint(equalTo: level1Hatches.trailingAnchor).isActive = true
        
        level3Hatches.translatesAutoresizingMaskIntoConstraints = false
        level3Hatches.topAnchor.constraint(equalTo: hatches3Button.topAnchor, constant: 6).isActive = true
        level3Hatches.bottomAnchor.constraint(equalTo: hatches3Button.bottomAnchor, constant: -6).isActive = true
        level3Hatches.leadingAnchor.constraint(equalTo: hatches3Button.leadingAnchor, constant: 306).isActive = true
        level3Hatches.trailingAnchor.constraint(equalTo: level1Hatches.trailingAnchor).isActive = true
        
        level1Cargo.translatesAutoresizingMaskIntoConstraints = false
        level1Cargo.topAnchor.constraint(equalTo: cargo1Button.topAnchor, constant: 6).isActive = true
        level1Cargo.bottomAnchor.constraint(equalTo: cargo1Button.bottomAnchor, constant: -6).isActive = true
        level1Cargo.leadingAnchor.constraint(equalTo: cargo1Button.leadingAnchor, constant: 306).isActive = true
        level1Cargo.trailingAnchor.constraint(equalTo: level1Hatches.trailingAnchor).isActive = true
        
        level2Cargo.translatesAutoresizingMaskIntoConstraints = false
        level2Cargo.topAnchor.constraint(equalTo: cargo2Button.topAnchor, constant: 6).isActive = true
        level2Cargo.bottomAnchor.constraint(equalTo: cargo2Button.bottomAnchor, constant: -6).isActive = true
        level2Cargo.leadingAnchor.constraint(equalTo: cargo2Button.leadingAnchor, constant: 306).isActive = true
        level2Cargo.trailingAnchor.constraint(equalTo: level1Hatches.trailingAnchor).isActive = true
        
        level3Cargo.translatesAutoresizingMaskIntoConstraints = false
        level3Cargo.topAnchor.constraint(equalTo: cargi3Button.topAnchor, constant: 6).isActive = true
        level3Cargo.bottomAnchor.constraint(equalTo: cargi3Button.bottomAnchor, constant: -6).isActive = true
        level3Cargo.leadingAnchor.constraint(equalTo: cargi3Button.leadingAnchor, constant: 306).isActive = true
        level3Cargo.trailingAnchor.constraint(equalTo: level1Hatches.trailingAnchor).isActive = true
        
        level1Climb.translatesAutoresizingMaskIntoConstraints = false
        level1Climb.topAnchor.constraint(equalTo: climb1Button.topAnchor, constant: 6).isActive = true
        level1Climb.bottomAnchor.constraint(equalTo: climb1Button.bottomAnchor, constant: -6).isActive = true
        level1Climb.leadingAnchor.constraint(equalTo: climb1Button.leadingAnchor, constant: 306).isActive = true
        level1Climb.trailingAnchor.constraint(equalTo: level1Hatches.trailingAnchor).isActive = true
        
        level2Climb.translatesAutoresizingMaskIntoConstraints = false
        level2Climb.topAnchor.constraint(equalTo: climb2Button.topAnchor, constant: 6).isActive = true
        level2Climb.bottomAnchor.constraint(equalTo: climb2Button.bottomAnchor, constant: -6).isActive = true
        level2Climb.leadingAnchor.constraint(equalTo: climb2Button.leadingAnchor, constant: 306).isActive = true
        level2Climb.trailingAnchor.constraint(equalTo: level1Hatches.trailingAnchor).isActive = true
        
        level3Climb.translatesAutoresizingMaskIntoConstraints = false
        level3Climb.topAnchor.constraint(equalTo: climb3Button.topAnchor, constant: 6).isActive = true
        level3Climb.bottomAnchor.constraint(equalTo: climb3Button.bottomAnchor, constant: -6).isActive = true
        level3Climb.leadingAnchor.constraint(equalTo: climb3Button.leadingAnchor, constant: 306).isActive = true
        level3Climb.trailingAnchor.constraint(equalTo: level1Hatches.trailingAnchor).isActive = true
        
        signInButton.translatesAutoresizingMaskIntoConstraints = false
        signInButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 16).isActive = true
        signInButton.bottomAnchor.constraint(equalTo: view.topAnchor, constant: 48).isActive = true
        signInButton.leadingAnchor.constraint(equalTo: view.trailingAnchor, constant: -64).isActive = true
        signInButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 16).isActive = true
        
        view.bringSubviewToFront(teamDropDown)
        view.bringSubviewToFront(matchDropDown)
        
        view.sendSubviewToBack(scrollView)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              
              withError error: Error!) {
        
        if let error = error {
            
            showAlert(title: "Authentication Error", message: error.localizedDescription)
            
            self.service.authorizer = nil
            
        } else {
            
            self.signInButton.isHidden = true
            self.output.isHidden = false
            self.level1Hatches.isHidden = false
            self.cell.isHidden = false
            self.sheetTitle.isHidden = false
            self.sheetID.isHidden = true
            self.warning.isHidden = true
            
            self.service.authorizer = user.authentication.fetcherAuthorizer()
            
            //listMajors()
            getNumberInB2()
            getNames()
            
            // The list of array to display. Can be changed dynamically
            matchDropDown.optionArray = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
            //Its Id Values and its optional
            matchDropDown.optionIds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
            // The the Closure returns Selected Index and String
            matchDropDown.didSelect{(selectedText , index ,id) in
                self.matchesArray = ["Selected String: \(selectedText) \n index: \(index)"]
                self.averageDivisor = selectedText
                if selectedText == "1" {
                    self.matchNumber.text = "B"
                } else if selectedText == "2" {
                    self.matchNumber.text = "C"
                } else if selectedText == "3" {
                    self.matchNumber.text = "D"
                } else if selectedText == "4" {
                    self.matchNumber.text = "E"
                } else if selectedText == "5" {
                    self.matchNumber.text = "F"
                } else if selectedText == "6" {
                    self.matchNumber.text = "G"
                } else if selectedText == "7" {
                    self.matchNumber.text = "H"
                } else if selectedText == "8" {
                    self.matchNumber.text = "I"
                } else if selectedText == "9" {
                    self.matchNumber.text = "J"
                } else if selectedText == "10" {
                    self.matchNumber.text = "K"
                } else if selectedText == "11" {
                    self.matchNumber.text = "L"
                } else if selectedText == "12" {
                    self.matchNumber.text = "M"
                }
                //self.matchNumber.text = selectedText
            }
        }
        
    }
    
    // Display (in the UITextView) the names and majors of students in a sample
    
    // spreadsheet:
    //https://docs.google.com/spreadsheets/d/1co47_hf1MsoSHYowge2YHZu8RW0KycxzF-elQ6LI4QY/edit#gid=0
    
    func listMajors() {
        
        output.text = "Getting sheet data..."
        
        let range = "Sheet1!A1:D6"
        
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range)
        
        service.executeQuery(query, delegate: self, didFinish: #selector(displayResultWithTicket(ticket:finishedWithObject:error:)))
        
    }
    
    // Process the response and display output
    
    @objc func displayResultWithTicket(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
        var majorsString = ""
        
        //let rows: Array = [[a,2,3,4],[1,2,3,4], [3,3,3,3]]
        let rows = result.values!
        
        if rows.isEmpty {
            output.text = "No data found."
            
            return
        }
        
        //majorsString += "Name, Major:\n"
        
        for row in rows {
            //let name = row[0]
            
            let major = row[0]
            
            majorsString += "\(major)"
            
        }
        
        //output.text = majorsString
        
        numberOfSheets = Int(majorsString)!
        
    }
    
    @objc func updateResultWithTicket(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
    }
    
    @objc func forGettingNames(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
        //var a = "w"
        var majorsString = ""
        
        //let rows: Array = [[a,2,3,4],[1,2,3,4], [3,3,3,3]]
        let rows = result.values!
        
        if rows.isEmpty {
            output.text = "No data found."
            
            return
        }
        array.removeAll()
        arrayOfSheets.removeAll()
        for row in rows {

            idGenerator += 1
            
            let name = row[0]
            
            let major = row[0]
            
            arrayOfSheets.append("\(major)")
            majorsString += "\(name), "
            array.append(idGenerator)
        }
        
        // The list of array to display. Can be changed dynamically
        teamDropDown.optionArray = arrayOfSheets
        //Its Id Values and its optional
        teamDropDown.optionIds = array
        // The the Closure returns Selected Index and String
        teamDropDown.didSelect{(selectedText , index ,id) in
            self.arrayOfSheets = ["Selected String: \(selectedText) \n index: \(index)"]
            self.sheetTitle.text = selectedText
        }
        
    }
    
    // Helper for showing an alert
    
    func showAlert(title : String, message: String) {
        
        let alert = UIAlertController(
            
            title: title,
            
            message: message,
            
            preferredStyle: UIAlertController.Style.alert
            
        )
        
        let ok = UIAlertAction(
            
            title: "OK",
            
            style: UIAlertAction.Style.default,
            
            handler: nil
            
        )
        
        alert.addAction(ok)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func updateValuesButton(_ sender: Any) {
        updateHatches()
        updateAverageDivisorForHatches()
        updateCargo()
        updateAverageDivisorForCargo()
        updateClimb()
        updateAverageDivisorForClimb()
    }
    
    func addSheetNameToName() {
        let range = "Name!A\(numberOfSheets)"
        
        let valueRange = GTLRSheets_ValueRange.init()
        valueRange.values = [ [String(sheetTitle.text)] ]
        
        let queryUpdate = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange, spreadsheetId: spreadsheetId, range: range)
        queryUpdate.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
        
    }
    
    func getNumberInB2() {
        let range = "Name!B1"
        
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range)
        
        service.executeQuery(query, delegate: self, didFinish: #selector(displayResultWithTicket(ticket:finishedWithObject:error:)))

    }
    
    func updateNumberInB2() {
        let range = "Name!B1"
        
        let valueRange = GTLRSheets_ValueRange.init()
        valueRange.values = [ [numberOfSheets] ]
        
        let queryUpdate = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange, spreadsheetId: spreadsheetId, range: range)
        queryUpdate.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
        
    }
    
    func getNames() {
        let range = "Name!A1:B500"
        
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range)
        
        service.executeQuery(query, delegate: self, didFinish: #selector(forGettingNames(ticket:finishedWithObject:error:)))
        
    }
    
    func updateHatches() {
        let range = "\(String(sheetTitle.text))!\(String(matchNumber.text))4"
        
        let valueRange = GTLRSheets_ValueRange.init()
        valueRange.values = [ [String(level1Hatches.text)] ]
        
        let queryUpdate = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange, spreadsheetId: spreadsheetId, range: range)
        queryUpdate.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
        
        let range2 = "\(String(sheetTitle.text))!\(String(matchNumber.text))5"
        
        let valueRangeLvl2 = GTLRSheets_ValueRange.init()
        valueRangeLvl2.values = [ [String(level2Hatches.text)] ]
        
        let queryUpdate2 = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRangeLvl2, spreadsheetId: spreadsheetId, range: range2)
        queryUpdate2.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate2, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))

        
        let range3 = "\(String(sheetTitle.text))!\(String(matchNumber.text))6"
        
        let valueRangeLvl3 = GTLRSheets_ValueRange.init()
        valueRangeLvl3.values = [ [String(level3Hatches.text)] ]
        
        let queryUpdate3 = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRangeLvl3, spreadsheetId: spreadsheetId, range: range3)
        queryUpdate3.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate3, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
    }
    
    func updateCargo() {
        let range = "\(String(sheetTitle.text))!\(String(matchNumber.text))9"
        
        let valueRange = GTLRSheets_ValueRange.init()
        valueRange.values = [ [String(level1Cargo.text)] ]
        
        let queryUpdate = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange, spreadsheetId: spreadsheetId, range: range)
        queryUpdate.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
        
        let range2 = "\(String(sheetTitle.text))!\(String(matchNumber.text))10"
        
        let valueRangeLvl2 = GTLRSheets_ValueRange.init()
        valueRangeLvl2.values = [ [String(level2Cargo.text)] ]
        
        let queryUpdate2 = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRangeLvl2, spreadsheetId: spreadsheetId, range: range2)
        queryUpdate2.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate2, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
        
        let range3 = "\(String(sheetTitle.text))!\(String(matchNumber.text))11"
        
        let valueRangeLvl3 = GTLRSheets_ValueRange.init()
        valueRangeLvl3.values = [ [String(level3Cargo.text)] ]
        
        let queryUpdate3 = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRangeLvl3, spreadsheetId: spreadsheetId, range: range3)
        queryUpdate3.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate3, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
    }
    
    func updateClimb() {
        let range = "\(String(sheetTitle.text))!\(String(matchNumber.text))14"
        
        let valueRange = GTLRSheets_ValueRange.init()
        valueRange.values = [ [String(level1Climb.text)] ]
        
        let queryUpdate = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange, spreadsheetId: spreadsheetId, range: range)
        queryUpdate.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
        
        let range2 = "\(String(sheetTitle.text))!\(String(matchNumber.text))15"
        
        let valueRangeLvl2 = GTLRSheets_ValueRange.init()
        valueRangeLvl2.values = [ [String(level2Climb.text)] ]
        
        let queryUpdate2 = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRangeLvl2, spreadsheetId: spreadsheetId, range: range2)
        queryUpdate2.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate2, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
        
        let range3 = "\(String(sheetTitle.text))!\(String(matchNumber.text))16"
        
        let valueRangeLvl3 = GTLRSheets_ValueRange.init()
        valueRangeLvl3.values = [ [String(level3Climb.text)] ]
        
        let queryUpdate3 = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRangeLvl3, spreadsheetId: spreadsheetId, range: range3)
        queryUpdate3.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate3, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
    }
    
    @IBAction func level1HatchesButton(_ sender: Any) {
        lvl1H += 1
        level1Hatches.text = String(lvl1H)
    }
    
    @IBAction func level2HatchesButton(_ sender: Any) {
        lvl2H += 1
        level2Hatches.text = String(lvl2H)
    }
  
    @IBAction func level3HatchesButton(_ sender: Any) {
        lvl3H += 1
        level3Hatches.text = String(lvl3H)
    }
    
    @IBAction func level1CargoButton(_ sender: Any) {
        lvl1C += 1
        level1Cargo.text = String(lvl1C)
    }
    
    @IBAction func level2CargoButton(_ sender: Any) {
        lvl2C += 1
        level2Cargo.text = String(lvl2C)
    }
   
    @IBAction func level3CargoButton(_ sender: Any) {
        lvl3C += 1
        level3Cargo.text = String(lvl3C)
    }

    @IBAction func level1ClimbButton(_ sender: Any) {
        if lvl3Cl == 0 && lvl2Cl == 0 && lvl1Cl ==  0 {
            lvl1Cl += 1
            level1Climb.text = String(lvl1Cl)
        }
    }
    
    @IBAction func level2ClimbButton(_ sender: Any) {
        if lvl3Cl == 0 && lvl2Cl == 0 && lvl1Cl ==  0 {
            lvl2Cl += 2
            level2Climb.text = String(lvl2Cl)
        }
    }
    
    @IBAction func level3ClimbButton(_ sender: Any) {
        if lvl3Cl == 0 && lvl2Cl == 0 && lvl1Cl ==  0 {
            lvl3Cl += 3
            level3Climb.text = String(lvl3Cl)
        }
    }
    
    @IBAction func hatches1Sub(_ sender: Any) {
        if lvl1H > 0 {
            lvl1H -= 1
            level1Hatches.text = String(lvl1H)
        }
    }
    
    @IBAction func hatches2Sub(_ sender: Any) {
        if lvl2H > 0 {
            lvl2H -= 1
            level2Hatches.text = String(lvl2H)
        }
    }
    
    @IBAction func hatches3Sub(_ sender: Any) {
        if lvl3H > 0 {
            lvl3H -= 1
            level3Hatches.text = String(lvl3H)
        }
    }
    
    @IBAction func cargo1Sub(_ sender: Any) {
        if lvl1C > 0 {
            lvl1C -= 1
            level1Cargo.text = String(lvl1C)
        }
    }
    
    @IBAction func cargo2Sub(_ sender: Any) {
        if lvl2C > 0 {
            lvl2C -= 1
            level2Cargo.text = String(lvl2C)
        }
    }
    
    @IBAction func cargo3Sub(_ sender: Any) {
        if lvl3C > 0 {
            lvl3C -= 1
            level3Cargo.text = String(lvl3C)
        }
    }
    
    @IBAction func climb1Sub(_ sender: Any) {
        if lvl1Cl == 1 {
            lvl1Cl -= 1
            level1Climb.text = String(lvl1Cl)
        }
    }
    
    @IBAction func climb2Sub(_ sender: Any) {
        if lvl2Cl == 2 {
            lvl2Cl -= 2
            level2Climb.text = String(lvl2Cl)
        }
    }
    
    @IBAction func climb3Sub(_ sender: Any) {
        if lvl3Cl == 3 {
            lvl3Cl -= 3
            level3Climb.text = String(lvl3Cl)
        }
    }
    
    func updateAverageDivisorForHatches() {
        let range = "\(String(sheetTitle.text))!C18"
        
        let valueRange = GTLRSheets_ValueRange.init()
        valueRange.values = [ ["=TRUNC(SUM(B4:M6))/\(averageDivisor)"] ]
        
        let queryUpdate = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange, spreadsheetId: spreadsheetId, range: range)
        queryUpdate.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
    }
    
    func updateAverageDivisorForCargo() {
        let range = "\(String(sheetTitle.text))!C19"
        
        let valueRange = GTLRSheets_ValueRange.init()
        valueRange.values = [ ["=TRUNC(SUM(B9:M11))/\(averageDivisor)"] ]
        
        let queryUpdate = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange, spreadsheetId: spreadsheetId, range: range)
        queryUpdate.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
    }
    
    func updateAverageDivisorForClimb() {
        let range = "\(String(sheetTitle.text))!C20"
        
        let valueRange = GTLRSheets_ValueRange.init()
        valueRange.values = [ ["=TRUNC(SUM(B14:M16))/\(averageDivisor)"] ]
        
        let queryUpdate = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange, spreadsheetId: spreadsheetId, range: range)
        queryUpdate.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
    }
    
}
