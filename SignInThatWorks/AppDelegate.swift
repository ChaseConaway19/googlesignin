//
//  AppDelegate.swift
//  SignInThatWorks
//
//  Created by Chase Conaway on 4/29/19.
//  Copyright © 2019 Chase Conaway. All rights reserved.
//

import Google

import GoogleSignIn

import UIKit

import Firebase

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    
    var window: UIWindow?
    
    func application(_ application: UIApplication,
                     
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Initialize sign-in
        
        GIDSignIn.sharedInstance().clientID = "100725033940-2vk4gplg802804qq9249o1v1u43kdorl.apps.googleusercontent.com"
        
        GIDSignIn.sharedInstance().delegate = self as? GIDSignInDelegate
        
        FIRApp.configure()
        
        
        
        return true
        
    }
    
    
    
    func applicationDidFinishLaunching(_ application: UIApplication) {
        
        // Initialize sign-in
        
        var configureError: NSError?
        
        GGLContext.sharedInstance().configureWithError(&configureError)
        
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
    }
    
    
    
    func application(_ application: UIApplication,
                     
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        return GIDSignIn.sharedInstance().handle(url,
                                                 
                                                 sourceApplication: sourceApplication,
                                                 
                                                 annotation: annotation)
        
    }
    
    
    
    @available(iOS 9.0, *)
    
    func application(_ app: UIApplication, open url: URL,
                     
                     options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        
        let sourceApplication = options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String
        
        let annotation = options[UIApplication.OpenURLOptionsKey.annotation]
        
        return GIDSignIn.sharedInstance().handle(url,
                                                 
                                                 sourceApplication: sourceApplication,
                                                 
                                                 annotation: annotation)
        
    }
    
    
    
}

