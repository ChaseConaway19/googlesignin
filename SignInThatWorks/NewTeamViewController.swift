//
//  NewTeamViewController.swift
//  SignInThatWorks
//
//  Created by Chase Conaway on 5/21/19.
//  Copyright © 2019 Chase Conaway. All rights reserved.
//

import Foundation
import GoogleAPIClientForREST
import iOSDropDown
import GoogleSignIn
import UIKit

class NewTeamViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {
    
    // If modifying these scopes, delete your previously saved credentials by
    
    // resetting the iOS simulator or uninstall the app.
    
    private let scopes = [kGTLRAuthScopeSheetsSpreadsheets]
    private let service = GTLRSheetsService()
    
    let signInButton = GIDSignInButton()
    
    let output = UITextView()
    let cell = UITextView()
    let sheetTitle = UITextView()
    let sheetID = UITextView()
    let warning = UITextView()
    var arrayOfSheets: Array<String> = []
    
    let spreadsheetId = globalSpreadsheetID
    
    var count = 1
    var idGenerator = 0
    var numberOfSheets = 0
    var sheet = 1
    var array: Array<Int> = []
    var matchesArray: Array<String> = []

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        //GIDSignIn.sharedInstance()?.signOut()
        
        // Configure Google Sign-in.
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = scopes
        GIDSignIn.sharedInstance().signInSilently()
        
        // Add the sign-in button.
        
        signInButton.center = CGPoint(x: 100, y: 100)
        
        view.addSubview(signInButton)
        
        // Add a UITextView to display output.
        output.frame = CGRect(x: 0, y: 0, width: 100, height: 150)
        output.isEditable = false
        output.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        output.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        output.center = CGPoint(x: 300, y: 100)
        output.isHidden = false
        output.backgroundColor = UIColor(red: 127/255.0, green: 128/255.0, blue: 131/255.0, alpha: 1.0)
        
        cell.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        cell.isEditable = true
        cell.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        cell.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        cell.isHidden = false
        cell.center = CGPoint(x: 200, y: 300)
        cell.textColor = UIColor.white
        cell.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        
        sheetTitle.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        sheetTitle.isEditable = true
        sheetTitle.textAlignment = .center
        sheetTitle.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
        sheetTitle.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        sheetTitle.isHidden = false
        //sheetTitle.center = CGPoint(x: 200, y: 200)
        sheetTitle.textColor = UIColor.white
        sheetTitle.font = UIFont(name: "System", size: 18)
        sheetTitle.font = UIFont.systemFont(ofSize: 40.0)
        sheetTitle.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        sheetTitle.keyboardType = .numberPad
        
        sheetID.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        sheetID.isEditable = true
        sheetID.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        sheetID.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        sheetID.isHidden = true
        sheetID.center = CGPoint(x: 100, y: 200)
        sheetID.textColor = UIColor.white
        sheetID.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        
        warning.frame = CGRect(x: 0, y: 0, width: 100, height: 25)
        warning.isEditable = false
        warning.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        warning.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        warning.isHidden = true
        warning.center = CGPoint(x: 100, y: 160)
        warning.text = "Numbers only!!"
        warning.textColor = UIColor.white
        warning.backgroundColor = UIColor.black
        
        //view.addSubview(output)
        //view.addSubview(cell)
        view.addSubview(sheetTitle)
        //view.addSubview(sheetID)
        //view.addSubview(warning)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              
              withError error: Error!) {
        
        if let error = error {
            
            showAlert(title: "Authentication Error", message: error.localizedDescription)
            
            self.service.authorizer = nil
            
        } else {
            
            self.signInButton.isHidden = true
            self.output.isHidden = false
            self.cell.isHidden = false
            self.sheetTitle.isHidden = false
            self.sheetID.isHidden = true
            self.warning.isHidden = true
            
            self.service.authorizer = user.authentication.fetcherAuthorizer()
            
            //listMajors()
            getNumberInB2()
            getNames()
            executeRepeatedly()
            
        }
        
    }
    
    override func viewWillLayoutSubviews() {
        sheetTitle.translatesAutoresizingMaskIntoConstraints = false
        sheetTitle.topAnchor.constraint(equalTo: view.centerYAnchor, constant: -50).isActive = true
        sheetTitle.bottomAnchor.constraint(equalTo: view.centerYAnchor, constant: 50).isActive = true
        sheetTitle.leadingAnchor.constraint(equalTo: view.centerXAnchor, constant: -150).isActive = true
        sheetTitle.trailingAnchor.constraint(equalTo: view.centerXAnchor, constant: 150).isActive = true
        
        signInButton.translatesAutoresizingMaskIntoConstraints = false
        signInButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 16).isActive = true
        signInButton.bottomAnchor.constraint(equalTo: view.topAnchor, constant: 48).isActive = true
        signInButton.leadingAnchor.constraint(equalTo: view.trailingAnchor, constant: -64).isActive = true
        signInButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 16).isActive = true
        
        view.bringSubviewToFront(sheetTitle)
    }
    
    // Display (in the UITextView) the names and majors of students in a sample
    
    // spreadsheet:
    //https://docs.google.com/spreadsheets/d/1co47_hf1MsoSHYowge2YHZu8RW0KycxzF-elQ6LI4QY/edit#gid=0
    
    func listMajors() {
        
        output.text = "Getting sheet data..."
        
        let range = "Sheet1!A1:D6"
        
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range)
        
        service.executeQuery(query, delegate: self, didFinish: #selector(displayResultWithTicket(ticket:finishedWithObject:error:)))
        
    }
    
    // Process the response and display output
    
    @objc func displayResultWithTicket(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
        var majorsString = ""
        
        //let rows: Array = [[a,2,3,4],[1,2,3,4], [3,3,3,3]]
        let rows = result.values!
        
        if rows.isEmpty {
            output.text = "No data found."
            
            return
        }
        
        //majorsString += "Name, Major:\n"
        
        for row in rows {
            //let name = row[0]
            
            let major = row[0]
            
            majorsString += "\(major)"
            
        }
        
        //output.text = majorsString
        
        numberOfSheets = Int(majorsString)!
        
    }
    
    @objc func updateResultWithTicket(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
    }
    
    @objc func forGettingNames(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
        //var a = "w"
        var majorsString = ""
        
        //let rows: Array = [[a,2,3,4],[1,2,3,4], [3,3,3,3]]
        let rows = result.values!
        
        if rows.isEmpty {
            output.text = "No data found."
            
            return
        }
        array.removeAll()
        arrayOfSheets.removeAll()
        for row in rows {
            
            idGenerator += 1
            
            let name = row[0]
            
            let major = row[0]
            
            arrayOfSheets.append("\(major)")
            majorsString += "\(name), "
            array.append(idGenerator)
        }
        
    }
    
    // Helper for showing an alert
    
    func showAlert(title : String, message: String) {
        
        let alert = UIAlertController(
            
            title: title,
            
            message: message,
            
            preferredStyle: UIAlertController.Style.alert
            
        )
        
        let ok = UIAlertAction(
            
            title: "OK",
            
            style: UIAlertAction.Style.default,
            
            handler: nil
            
        )
        
        alert.addAction(ok)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    func duplicateSheet() {
        let batchUpdate = GTLRSheets_BatchUpdateSpreadsheetRequest.init()
        let request = GTLRSheets_Request.init()
        let sheetRequest = GTLRSheets_DuplicateSheetRequest.init()
        
        if sheetTitle.text != "" {
            sheetRequest.newSheetName = "\(String(sheetTitle.text))"
        }
        
        sheetRequest.sourceSheetId = 2910 as NSNumber?
        request.duplicateSheet = sheetRequest
        
        batchUpdate.requests = [request]
        
        let duplicateQuery = GTLRSheetsQuery_SpreadsheetsBatchUpdate.query(withObject: batchUpdate, spreadsheetId: spreadsheetId)
        
        service.executeQuery(duplicateQuery, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
        
    }
    
    @IBAction func duplicateSheet(_ sender: Any) {
        numberOfSheets += 1
        updateNumberInB2()
        addSheetNameToName()
        duplicateSheet()
        sleep(1)
        getNames()
    }
    
    func addSheetNameToName() {
        let range = "Name!A\(numberOfSheets)"
        
        let valueRange = GTLRSheets_ValueRange.init()
        valueRange.values = [ [String(sheetTitle.text)] ]
        
        let queryUpdate = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange, spreadsheetId: spreadsheetId, range: range)
        queryUpdate.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
        
    }
    
    func getNumberInB2() {
        let range = "Name!B1"
        
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range)
        
        service.executeQuery(query, delegate: self, didFinish: #selector(displayResultWithTicket(ticket:finishedWithObject:error:)))
        
    }
    
    func updateNumberInB2() {
        let range = "Name!B1"
        
        let valueRange = GTLRSheets_ValueRange.init()
        valueRange.values = [ [numberOfSheets] ]
        
        let queryUpdate = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange, spreadsheetId: spreadsheetId, range: range)
        queryUpdate.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
        
    }
    
    func getNames() {
        let range = "Name!A1:B500"
        
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range)
        
        service.executeQuery(query, delegate: self, didFinish: #selector(forGettingNames(ticket:finishedWithObject:error:)))
        
    }
    
    private func executeRepeatedly() {
        getNumberInB2()
        //getNames()
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0) { [weak self] in
            self?.executeRepeatedly()
        }
    }
    
}
