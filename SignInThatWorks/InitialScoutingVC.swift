//
//  InitialScoutingVC.swift
//  SignInThatWorks
//
//  Created by Chase Conaway on 5/28/19.
//  Copyright © 2019 Chase Conaway. All rights reserved.
//

import Foundation
import GoogleAPIClientForREST
import iOSDropDown
import GoogleSignIn
import UIKit

class InitialScoutingVC: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate, UITextViewDelegate, UIGestureRecognizerDelegate {
    
    private let scopes = [kGTLRAuthScopeSheetsSpreadsheets]
    private let service = GTLRSheetsService()
    
    let signInButton = GIDSignInButton()
    @IBOutlet weak var teamDropDown: DropDown!
    @IBOutlet weak var proclaimedDefense: UILabel!
    @IBOutlet weak var proclaimedHatches: UILabel!
    @IBOutlet weak var proclaimedCargo: UILabel!
    @IBOutlet weak var proclaimedClimbing: UILabel!
    
    @IBOutlet weak var proclaimedSpecialties: UILabel!
    
    let sheetName = UITextView()
    let defense = UITextView()
    let hatchesView = UITextView()
    let cargo = UITextView()
    let climbing = UITextView()
    let specialties = UITextView()
    
    let spreadsheetId = globalSpreadsheetID
    
    var count = 0
    var arrayOfSheets: Array<String> = []
    var numberOfSheets = 1
    var array: Array<Int> = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        //GIDSignIn.sharedInstance()?.signOut()
        
        // Configure Google Sign-in.
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = scopes
        GIDSignIn.sharedInstance().signInSilently()
        
        // Add the sign-in button.
        
        signInButton.center = CGPoint(x: 100, y: 100)
        signInButton.isHidden = true
        
        view.addSubview(signInButton)
        
        defense.frame = CGRect(x: 0, y: 0, width: 100, height: 20)
        defense.isEditable = true
        defense.textAlignment = .center
        defense.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        defense.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        //defense.center = CGPoint(x: 160, y: 474)
        defense.textColor = UIColor.white
        defense.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        defense.isHidden = false
        defense.text = "0"
        defense.keyboardType = .numberPad
        
        hatchesView.frame = CGRect(x: 0, y: 0, width: 100, height: 20)
        hatchesView.isEditable = true
        hatchesView.textAlignment = .center
        hatchesView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        hatchesView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        //hatchesView.center = CGPoint(x: 220, y: 549)
        hatchesView.textColor = UIColor.white
        hatchesView.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        hatchesView.isHidden = false
        hatchesView.text = "0"
        hatchesView.keyboardType = .numberPad
        
        cargo.frame = CGRect(x: 0, y: 0, width: 100, height: 20)
        cargo.isEditable = true
        cargo.textAlignment = .center
        cargo.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        cargo.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        //cargo.center = CGPoint(x: 160, y: 586)
        cargo.textColor = UIColor.white
        cargo.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        cargo.isHidden = false
        cargo.text = "0"
        cargo.keyboardType = .numberPad
        
        climbing.frame = CGRect(x: 0, y: 0, width: 100, height: 20)
        climbing.isEditable = true
        climbing.textAlignment = .center
        climbing.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        climbing.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        //climbing.center = CGPoint(x: 160, y: 624)
        climbing.textColor = UIColor.white
        climbing.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        climbing.isHidden = false
        climbing.text = "0"
        climbing.keyboardType = .numberPad
        
        specialties.frame = CGRect(x: 0, y: 0, width: 150, height: 50)
        specialties.isEditable = true
        specialties.textAlignment = .center
        specialties.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        specialties.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        //specialties.center = CGPoint(x: 160, y: 700)
        specialties.textColor = UIColor.white
        specialties.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        specialties.isHidden = false
        
        sheetName.frame = CGRect(x: 0, y: 0, width: 100, height: 150)
        sheetName.isEditable = false
        //sheetName.center = CGPoint(x: 300, y: 300)
        sheetName.font = UIFont.systemFont(ofSize: 20.0)
        sheetName.backgroundColor = UIColor.blue
        sheetName.textColor = UIColor.white
        sheetName.isHidden = true
 
        view.addSubview(sheetName)
        view.addSubview(defense)
        view.addSubview(hatchesView)
        view.addSubview(cargo)
        view.addSubview(climbing)
        view.addSubview(specialties)

    }
    
    override func viewWillLayoutSubviews() {
        defense.translatesAutoresizingMaskIntoConstraints = false
        defense.topAnchor.constraint(equalTo: proclaimedDefense.topAnchor, constant: -6).isActive = true
        defense.bottomAnchor.constraint(equalTo: proclaimedDefense.bottomAnchor, constant: 6).isActive = true
        defense.leadingAnchor.constraint(equalTo: proclaimedDefense.leadingAnchor, constant: 90).isActive = true
        defense.trailingAnchor.constraint(equalTo: proclaimedHatches.trailingAnchor, constant: 100).isActive = true
        
        hatchesView.translatesAutoresizingMaskIntoConstraints = false
        hatchesView.topAnchor.constraint(equalTo: proclaimedHatches.topAnchor, constant: -6).isActive = true
        hatchesView.bottomAnchor.constraint(equalTo: proclaimedHatches.bottomAnchor, constant: 6).isActive = true
        hatchesView.leadingAnchor.constraint(equalTo: proclaimedHatches.leadingAnchor, constant: 90).isActive = true
        hatchesView.trailingAnchor.constraint(equalTo: defense.trailingAnchor).isActive = true
        
        cargo.translatesAutoresizingMaskIntoConstraints = false
        cargo.topAnchor.constraint(equalTo: proclaimedCargo.topAnchor, constant: -6).isActive = true
        cargo.bottomAnchor.constraint(equalTo: proclaimedCargo.bottomAnchor, constant: 6).isActive = true
        cargo.leadingAnchor.constraint(equalTo: proclaimedCargo.leadingAnchor, constant: 90).isActive = true
        cargo.trailingAnchor.constraint(equalTo: defense.trailingAnchor).isActive = true
        
        climbing.translatesAutoresizingMaskIntoConstraints = false
        climbing.topAnchor.constraint(equalTo: proclaimedClimbing.topAnchor, constant: -6).isActive = true
        climbing.bottomAnchor.constraint(equalTo: proclaimedClimbing.bottomAnchor, constant: 6).isActive = true
        climbing.leadingAnchor.constraint(equalTo: proclaimedClimbing.leadingAnchor, constant: 90).isActive = true
        climbing.trailingAnchor.constraint(equalTo: defense.trailingAnchor).isActive = true
        
        specialties.translatesAutoresizingMaskIntoConstraints = false
        specialties.topAnchor.constraint(equalTo: proclaimedSpecialties.topAnchor, constant: 32).isActive = true
        specialties.bottomAnchor.constraint(equalTo: proclaimedSpecialties.bottomAnchor, constant: 124).isActive = true
        specialties.leadingAnchor.constraint(equalTo: proclaimedSpecialties.leadingAnchor).isActive = true
        specialties.trailingAnchor.constraint(equalTo: proclaimedSpecialties.trailingAnchor).isActive = true
        
        signInButton.translatesAutoresizingMaskIntoConstraints = false
        signInButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 16).isActive = true
        signInButton.bottomAnchor.constraint(equalTo: view.topAnchor, constant: 48).isActive = true
        signInButton.leadingAnchor.constraint(equalTo: view.trailingAnchor, constant: -64).isActive = true
        signInButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 16).isActive = true
        
        view.bringSubviewToFront(teamDropDown)

    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              
              withError error: Error!) {
        
        if let error = error {
            
            showAlert(title: "Authentication Error", message: error.localizedDescription)
            
            self.service.authorizer = nil
            
        } else {
            
            self.signInButton.isHidden = true
            self.sheetName.isHidden = true
            self.defense.isHidden = false
            self.hatchesView.isHidden = false
            self.cargo.isHidden = false
            self.climbing.isHidden = false
            self.specialties.isHidden = false
            
            self.service.authorizer = user.authentication.fetcherAuthorizer()
            
            getNamesOfSheets()
            
        }
        
    }
    
    func showAlert(title : String, message: String) {
        
        let alert = UIAlertController(
            
            title: title,
            
            message: message,
            
            preferredStyle: UIAlertController.Style.alert
            
        )
        
        let ok = UIAlertAction(
            
            title: "OK",
            
            style: UIAlertAction.Style.default,
            
            handler: nil
            
        )
        
        alert.addAction(ok)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    @objc func forGettingNames(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
        //var a = "w"
        var majorsString = ""
        
        //let rows: Array = [[a,2,3,4],[1,2,3,4], [3,3,3,3]]
        let rows = result.values!
        
        if rows.isEmpty {
            sheetName.text = "No data found."
            
            return
        }
        
        for row in rows {
            count += 1
            
            let name = row[0]
            
            let major = row[0]
            
            arrayOfSheets.append("\(major)")
            majorsString += "\(name), "
            array.append(count)
        }
        
        // The list of array to display. Can be changed dynamically
        teamDropDown.optionArray = arrayOfSheets
        //Its Id Values and its optional
        teamDropDown.optionIds = array
        // The the Closure returns Selected Index and String
        teamDropDown.didSelect{(selectedText , index ,id) in
            self.arrayOfSheets = ["Selected String: \(selectedText) \n index: \(index)"]
            self.sheetName.text = selectedText
        }
        
    }
    
    @objc func updateResultWithTicket(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
    }
    
    func getNamesOfSheets() {
        let range = "Name!A1:F400"
        
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range)
        
        service.executeQuery(query, delegate: self, didFinish: #selector(forGettingNames(ticket:finishedWithObject:error:)))
        
    }
    
    @IBAction func updateInitialScouting(_ sender: Any) {
        updateInfoForScouting()
    }
    
    func updateInfoForScouting() {
        let range = "\(String(sheetName.text))!B23"
        
        let valueRange = GTLRSheets_ValueRange.init()
        valueRange.values = [ [String(defense.text)] ]
        
        let queryUpdate = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange, spreadsheetId: spreadsheetId, range: range)
        queryUpdate.valueInputOption = "USER_ENTERED"
            
        service.executeQuery(queryUpdate, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
        
        let range2 = "\(String(sheetName.text))!B25"
        
        let valueRange2 = GTLRSheets_ValueRange.init()
        valueRange2.values = [ [String(hatchesView.text)] ]
        
        let queryUpdate2 = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange2, spreadsheetId: spreadsheetId, range: range2)
        queryUpdate2.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate2, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
        
        let range3 = "\(String(sheetName.text))!B26"
        
        let valueRange3 = GTLRSheets_ValueRange.init()
        valueRange3.values = [ [String(cargo.text)] ]
        
        let queryUpdate3 = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange3, spreadsheetId: spreadsheetId, range: range3)
        queryUpdate3.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate3, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
        
        let range4 = "\(String(sheetName.text))!B27"
        
        let valueRange4 = GTLRSheets_ValueRange.init()
        valueRange4.values = [ [String(climbing.text)] ]
        
        let queryUpdate4 = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange4, spreadsheetId: spreadsheetId, range: range4)
        queryUpdate4.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate4, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))
        
        let range5 = "\(String(sheetName.text))!B28"
        
        let valueRange5 = GTLRSheets_ValueRange.init()
        valueRange5.values = [ [String(specialties.text)] ]
        
        let queryUpdate5 = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange5, spreadsheetId: spreadsheetId, range: range5)
        queryUpdate5.valueInputOption = "USER_ENTERED"
        
        service.executeQuery(queryUpdate5, delegate: self, didFinish: #selector(updateResultWithTicket(ticket:finishedWithObject:error:)))

    }

}
