//
//  DataViewController.swift
//  SignInThatWorks
//
//  Created by Chase Conaway on 5/9/19.
//  Copyright © 2019 Chase Conaway. All rights reserved.
//

import GoogleAPIClientForREST
import iOSDropDown
import GoogleSignIn
import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

class DataViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate, UITextViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var teamDropDown: DropDown!
    @IBOutlet weak var averageHatches: UILabel!
    @IBOutlet weak var averageCargo: UILabel!
    @IBOutlet weak var averageClimb: UILabel!
    @IBOutlet weak var proclaimedDefense: UILabel!
    @IBOutlet weak var proclaimedHatches: UILabel!
    @IBOutlet weak var proclaimedCargo: UILabel!
    @IBOutlet weak var proclaimedClimbing: UILabel!
    @IBOutlet weak var proclaimedSpecialties: UILabel!
    
    // If modifying these scopes, delete your previously saved credentials by`
    
    // resetting the iOS simulator or uninstall the app.
    
    private let scopes = [kGTLRAuthScopeSheetsSpreadsheets]
    private let service = GTLRSheetsService()
    
    let signInButton = GIDSignInButton()
    
    let output = UITextView()
    let sheetName = UITextView()
    let avgCargo = UITextView()
    let climb = UITextView()
    let defense = UITextView()
    let hatchesView = UITextView()
    let cargo = UITextView()
    let climbing = UITextView()
    let specialties = UITextView()
    let fontSize: CGFloat = 17.0
    let fontSize2: CGFloat = 16.0
    let topOffset: CGFloat = -2
    let topOffset2: CGFloat = -5
    let topConstraint: CGFloat = 24
    let bottomConstraint: CGFloat = 32
    let spreadsheetId = globalSpreadsheetID
    
    var count = 0
    var arrayOfSheets: Array<String> = []
    var numberOfSheets = 1
    var array: Array<Int> = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        // Configure Google Sign-in.
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = scopes
        GIDSignIn.sharedInstance().signInSilently()
        
        // Add the sign-in button.
        
        signInButton.center = CGPoint(x: 100, y: 100)
        
        view.addSubview(signInButton)
        
        // Add a UITextView to display output.
        output.frame = CGRect(x: 0, y: 0, width: 100, height: 30)
        output.isEditable = false
        output.textAlignment = .center
        output.contentInset = UIEdgeInsets(top: topOffset2, left: 0, bottom: 0, right: 0)
        output.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        output.font = UIFont.systemFont(ofSize: fontSize)
        //output.center = CGPoint(x: 84, y: 220)
        output.textColor = UIColor.white
        output.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        output.isHidden = false
        output.text = "0"
        
        avgCargo.frame = CGRect(x: 0, y: 0, width: 100, height: 30)
        avgCargo.isEditable = false
        avgCargo.textAlignment = .center
        avgCargo.contentInset = UIEdgeInsets(top: topOffset2, left: 0, bottom: 0, right: 0)
        avgCargo.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        avgCargo.font = UIFont.systemFont(ofSize: fontSize)
        //avgCargo.center = CGPoint(x: 84, y: 304)
        avgCargo.textColor = UIColor.white
        avgCargo.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        avgCargo.isHidden = false
        avgCargo.text = "0"
        
        climb.frame = CGRect(x: 0, y: 0, width: 100, height: 30)
        climb.isEditable = false
        climb.textAlignment = .center
        climb.contentInset = UIEdgeInsets(top: topOffset2, left: 0, bottom: 0, right: 0)
        climb.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        climb.font = UIFont.systemFont(ofSize: fontSize)
        //climb.center = CGPoint(x: 84, y: 388)
        climb.textColor = UIColor.white
        climb.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        climb.isHidden = false
        climb.text = "0"
        
        defense.frame = CGRect(x: 0, y: 0, width: 100, height: 20)
        defense.isEditable = false
        defense.textAlignment = .center
        defense.contentInset = UIEdgeInsets(top: topOffset2, left: 0, bottom: 0, right: 0)
        defense.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        defense.font = UIFont.systemFont(ofSize: fontSize)
        //defense.center = CGPoint(x: 160, y: 474)
        defense.textColor = UIColor.white
        defense.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        defense.isHidden = false
        defense.text = "0"
        
        hatchesView.frame = CGRect(x: 0, y: 0, width: 100, height: 20)
        hatchesView.isEditable = false
        hatchesView.textAlignment = .center
        hatchesView.contentInset = UIEdgeInsets(top: topOffset2, left: 0, bottom: 0, right: 0)
        hatchesView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        hatchesView.font = UIFont.systemFont(ofSize: fontSize)
        //hatchesView.center = CGPoint(x: 220, y: 549)
        hatchesView.textColor = UIColor.white
        hatchesView.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        hatchesView.isHidden = false
        hatchesView.text = "0"
        
        cargo.frame = CGRect(x: 0, y: 0, width: 100, height: 20)
        cargo.isEditable = false
        cargo.textAlignment = .center
        cargo.contentInset = UIEdgeInsets(top: topOffset2, left: 0, bottom: 0, right: 0)
        cargo.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        cargo.font = UIFont.systemFont(ofSize: fontSize)
        //cargo.center = CGPoint(x: 160, y: 586)
        cargo.textColor = UIColor.white
        cargo.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        cargo.isHidden = false
        cargo.text = "0"
        
        climbing.frame = CGRect(x: 0, y: 0, width: 100, height: 20)
        climbing.isEditable = false
        climbing.textAlignment = .center
        climbing.contentInset = UIEdgeInsets(top: topOffset2, left: 0, bottom: 0, right: 0)
        climbing.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        climbing.font = UIFont.systemFont(ofSize: fontSize)
        //climbing.center = CGPoint(x: 160, y: 624)
        climbing.textColor = UIColor.white
        climbing.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        climbing.isHidden = false
        climbing.text = "0"
        
        specialties.frame = CGRect(x: 0, y: 0, width: 150, height: 50)
        specialties.isEditable = false
        specialties.textAlignment = .center
        specialties.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        specialties.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        specialties.font = UIFont.systemFont(ofSize: 15.0)
        //specialties.center = CGPoint(x: 160, y: 700)
        specialties.textColor = UIColor.white
        specialties.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        specialties.isHidden = false
        specialties.text = "0"
        
        sheetName.frame = CGRect(x: 0, y: 0, width: 100, height: 150)
        sheetName.isEditable = false
        sheetName.center = CGPoint(x: 300, y: 300)
        sheetName.backgroundColor = UIColor.blue
        sheetName.textColor = UIColor.white
        sheetName.isHidden = true
        
        view.addSubview(output)
        view.addSubview(sheetName)
        view.addSubview(avgCargo)
        view.addSubview(climb)
        view.addSubview(defense)
        view.addSubview(hatchesView)
        view.addSubview(cargo)
        view.addSubview(climbing)
        view.addSubview(specialties)
        
    }
    
    override func viewWillLayoutSubviews() {
        output.translatesAutoresizingMaskIntoConstraints = false
        output.topAnchor.constraint(equalTo: averageHatches.topAnchor, constant: topConstraint).isActive = true
        output.bottomAnchor.constraint(equalTo: averageHatches.bottomAnchor, constant: bottomConstraint).isActive = true
        output.leadingAnchor.constraint(equalTo: averageHatches.leadingAnchor, constant: 20).isActive = true
        output.trailingAnchor.constraint(equalTo: averageHatches.trailingAnchor, constant: 202).isActive = true
        
        avgCargo.translatesAutoresizingMaskIntoConstraints = false
        avgCargo.topAnchor.constraint(equalTo: averageCargo.topAnchor, constant: topConstraint).isActive = true
        avgCargo.bottomAnchor.constraint(equalTo: averageCargo.bottomAnchor, constant: bottomConstraint).isActive = true
        avgCargo.leadingAnchor.constraint(equalTo: output.leadingAnchor).isActive = true
        avgCargo.trailingAnchor.constraint(equalTo: output.trailingAnchor).isActive = true
        
        climb.translatesAutoresizingMaskIntoConstraints = false
        climb.topAnchor.constraint(equalTo: averageClimb.topAnchor, constant: topConstraint).isActive = true
        climb.bottomAnchor.constraint(equalTo: averageClimb.bottomAnchor, constant: bottomConstraint).isActive = true
        climb.leadingAnchor.constraint(equalTo: output.leadingAnchor).isActive = true
        climb.trailingAnchor.constraint(equalTo: output.trailingAnchor).isActive = true
        
        defense.translatesAutoresizingMaskIntoConstraints = false
        defense.topAnchor.constraint(equalTo: proclaimedDefense.topAnchor, constant: -6).isActive = true
        defense.bottomAnchor.constraint(equalTo: proclaimedDefense.bottomAnchor, constant: 18).isActive = true
        defense.leadingAnchor.constraint(equalTo: proclaimedDefense.leadingAnchor, constant: 90).isActive = true
        defense.trailingAnchor.constraint(equalTo: output.trailingAnchor, constant: 25).isActive = true
        
        hatchesView.translatesAutoresizingMaskIntoConstraints = false
        hatchesView.topAnchor.constraint(equalTo: proclaimedHatches.topAnchor, constant: -6).isActive = true
        hatchesView.bottomAnchor.constraint(equalTo: proclaimedHatches.bottomAnchor, constant: 6).isActive = true
        hatchesView.leadingAnchor.constraint(equalTo: proclaimedHatches.leadingAnchor, constant: 90).isActive = true
        hatchesView.trailingAnchor.constraint(equalTo: defense.trailingAnchor, constant: 10).isActive = true
        
        cargo.translatesAutoresizingMaskIntoConstraints = false
        cargo.topAnchor.constraint(equalTo: proclaimedCargo.topAnchor, constant: -6).isActive = true
        cargo.bottomAnchor.constraint(equalTo: proclaimedCargo.bottomAnchor, constant: 6).isActive = true
        cargo.leadingAnchor.constraint(equalTo: proclaimedCargo.leadingAnchor, constant: 90).isActive = true
        cargo.trailingAnchor.constraint(equalTo: defense.trailingAnchor, constant: 10).isActive = true
        
        climbing.translatesAutoresizingMaskIntoConstraints = false
        climbing.topAnchor.constraint(equalTo: proclaimedClimbing.topAnchor, constant: -6).isActive = true
        climbing.bottomAnchor.constraint(equalTo: proclaimedClimbing.bottomAnchor, constant: 6).isActive = true
        climbing.leadingAnchor.constraint(equalTo: proclaimedClimbing.leadingAnchor, constant: 90).isActive = true
        climbing.trailingAnchor.constraint(equalTo: defense.trailingAnchor, constant: 10).isActive = true
        
        specialties.translatesAutoresizingMaskIntoConstraints = false
        specialties.topAnchor.constraint(equalTo: proclaimedSpecialties.topAnchor).isActive = true
        specialties.bottomAnchor.constraint(equalTo: proclaimedSpecialties.bottomAnchor, constant: 80).isActive = true
        specialties.leadingAnchor.constraint(equalTo: proclaimedSpecialties.leadingAnchor, constant: 120).isActive = true
        specialties.trailingAnchor.constraint(equalTo: proclaimedSpecialties.trailingAnchor, constant: 250).isActive = true
        
        signInButton.translatesAutoresizingMaskIntoConstraints = false
        signInButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 16).isActive = true
        signInButton.bottomAnchor.constraint(equalTo: view.topAnchor, constant: 48).isActive = true
        signInButton.leadingAnchor.constraint(equalTo: view.trailingAnchor, constant: -64).isActive = true
        signInButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 16).isActive = true
        
        view.bringSubviewToFront(teamDropDown)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              
              withError error: Error!) {
        
        if let error = error {
            
            showAlert(title: "Authentication Error", message: error.localizedDescription)
            
            self.service.authorizer = nil
            
        } else {
            
            self.signInButton.isHidden = true
            self.output.isHidden = false
            self.sheetName.isHidden = true
            self.avgCargo.isHidden = false
            self.cargo.isHidden = false
            self.climb.isHidden = false
            self.climbing.isHidden = false
            self.defense.isHidden = false
            self.hatchesView.isHidden = false
            self.avgCargo.isHidden = false
            self.specialties.isHidden = false

            self.service.authorizer = user.authentication.fetcherAuthorizer()

            
            getNumberOfSheets()
            getNamesOfSheets()
            
        }
        
    }
    
    // Display (in the UITextView) the names and majors of students in a sample
    
    // spreadsheet:
    //https://docs.google.com/spreadsheets/d/1co47_hf1MsoSHYowge2YHZu8RW0KycxzF-elQ6LI4QY/edit#gid=0
    
    func hatches() {
        
        output.text = "Getting sheet data..."
        
        let range = "\(String(sheetName.text))!C18"
        
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range)
        
        service.executeQuery(query, delegate: self, didFinish: #selector(forListMajors(ticket:finishedWithObject:error:)))
        
    }
    
    // Process the response and display output
    
    @objc func displayResultWithTicket(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
        //var a = "w"
        var majorsString = ""
        
        //let rows: Array = [[1,2,3,4],[1,2,3,4], [3,3,3,3]]
        let rows = result.values!
        
        if rows.isEmpty {
            output.text = "No data found."
            
            return
        }
        
        //majorsString += "Name, Major:\n"
        
        for row in rows {
            
            //let name = row[0]
            
            let major = row[0]
            
            majorsString += "\(major)"
            
        }
        
        //output.text = majorsString
        
        numberOfSheets = Int(majorsString)!
        print("Yee yee \(numberOfSheets)")
    }
    
    @objc func forGettingNames(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
        //var a = "w"
        var majorsString = ""
        
        //let rows: Array = [[a,2,3,4],[1,2,3,4], [3,3,3,3]]
        let rows = result.values!
        
        if rows.isEmpty {
            output.text = "No data found."
            
            return
        }
        
        for row in rows {
            count += 1
            
            let name = row[0]
            
            let major = row[0]
            
            arrayOfSheets.append("\(major)")
            majorsString += "\(name), "
            array.append(count)
        }
        
        // The list of array to display. Can be changed dynamically
        teamDropDown.optionArray = arrayOfSheets
        //Its Id Values and its optional
        teamDropDown.optionIds = array
        // The the Closure returns Selected Index and String
        teamDropDown.didSelect{(selectedText , index ,id) in
        self.arrayOfSheets = ["Selected String: \(selectedText) \n index: \(index)"]
            self.sheetName.text = selectedText
        }
        
    }
    
    @objc func forListMajors(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
        //var a = "w"
        var majorsString = ""
        
        //let rows: Array = [[a,2,3,4],[1,2,3,4], [3,3,3,3]]
        let rows = result.values!
        
        if rows.isEmpty {
            output.text = "No data found."
            
            return
        }
        
        output.text = "Their average hatches per match is "
        
        for row in rows {
            
            let name = row[0]
            
            //let major = row[1]
            
            majorsString += "\(name)"
        }
        
        output.text += majorsString
        
    }
    
    @objc func forCargo(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
        //var a = "w"
        var majorsString = ""
        
        //let rows: Array = [[a,2,3,4],[1,2,3,4], [3,3,3,3]]
        let rows = result.values!
        
        if rows.isEmpty {
            avgCargo.text = "No data found."
            
            return
        }
        
        avgCargo.text = "Their average cargo per match is "
        
        for row in rows {
            
            let name = row[0]
            
            //let major = row[1]
            
            majorsString += "\(name)"
        }
        
        avgCargo.text += majorsString
        
    }
    
    @objc func forClimb(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
        //var a = "w"
        var majorsString = ""
        
        //let rows: Array = [[a,2,3,4],[1,2,3,4], [3,3,3,3]]
        let rows = result.values!
        
        if rows.isEmpty {
            climb.text = "No data found."
            
            return
        }
        
        climb.text = "Their average cargo per match is "
        
        for row in rows {
            
            let name = row[0]
            
            //let major = row[1]
            
            majorsString += "\(name)"
        }
        
        climb.text += majorsString
        
    }
    
    @objc func forDefense(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
        //var a = "w"
        var majorsString = ""
        
        //let rows: Array = [[a,2,3,4],[1,2,3,4], [3,3,3,3]]
        let rows = result.values!
        
        if rows.isEmpty {
            defense.text = "No data found."
            
            return
        }
        
        defense.text = "Their defense, on a scale from 1-10, is probably "
        
        for row in rows {
            
            let name = row[0]
            
            //let major = row[1]
            
            majorsString += "\(name)"
        }
        
        defense.text += majorsString
        
    }
    
    @objc func forHatches(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
        //var a = "w"
        var majorsString = ""
        
        //let rows: Array = [[a,2,3,4],[1,2,3,4], [3,3,3,3]]
        let rows = result.values!
        
        if rows.isEmpty {
            hatchesView.text = "No data found."
            
            return
        }
        
        hatchesView.text = "They say their hatches per match is "
        
        for row in rows {
            
            let name = row[0]
            
            //let major = row[1]
            
            majorsString += "\(name)"
        }
        
        hatchesView
            .text += majorsString
        
    }
    
    @objc func forInitialScoutCargo(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
        //var a = "w"
        var majorsString = ""
        
        //let rows: Array = [[a,2,3,4],[1,2,3,4], [3,3,3,3]]
        let rows = result.values!
        
        if rows.isEmpty {
            cargo.text = "No data found."
            
            return
        }
        
        cargo.text = "They say their cargo per match is "
        
        for row in rows {
            
            let name = row[0]
            
            //let major = row[1]
            
            majorsString += "\(name)"
        }
        
        cargo.text += majorsString
        
    }
    
    @objc func forInitialScoutClimb(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
        //var a = "w"
        var majorsString = ""
        
        //let rows: Array = [[a,2,3,4],[1,2,3,4], [3,3,3,3]]
        let rows = result.values!
        
        if rows.isEmpty {
            climbing.text = "No data found."
            
            return
        }
        
        climbing.text = "They say their climb is "
        
        for row in rows {
            
            let name = row[0]
            
            //let major = row[1]
            
            majorsString += "\(name)"
        }
        
        climbing.text += majorsString
        
    }
    
    @objc func forInitialScoutSpecialty(ticket: GTLRServiceTicket, finishedWithObject result : GTLRSheets_ValueRange, error : NSError?) {
        
        if let error = error {
            
            showAlert(title: "Error", message: error.localizedDescription)
            
            return
            
        }
        
        //var a = "w"
        var majorsString = ""
        
        //let rows: Array = [[a,2,3,4],[1,2,3,4], [3,3,3,3]]
        let rows = result.values!
        
        if rows.isEmpty {
            specialties.text = "No data found."
            
            return
        }
        
        specialties.text = "They say their specialties include: \n "
        
        for row in rows {
            
            let name = row[0]
            
            //let major = row[1]
            
            majorsString += "\(name)"
        }
        
        specialties.text += majorsString
        
    }
    
    // Helper for showing an alert
    
    func showAlert(title : String, message: String) {
        
        let alert = UIAlertController(
            
            title: title,
            message: message,
            preferredStyle: UIAlertController.Style.alert
            
        )
        
        let ok = UIAlertAction(
            
            title: "OK",
            style: UIAlertAction.Style.default,
            handler: nil
            
        )
        
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func showInfo(_ sender: Any) {
        hatches()
        cargoMatch()
        getClimb()
        getInitialScout()
    }
    
    func getNumberOfSheets() {
        let range = "Name!B1"
        
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range)
        
        service.executeQuery(query, delegate: self, didFinish: #selector(displayResultWithTicket(ticket:finishedWithObject:error:)))
    }
    
    func getNamesOfSheets() {
        let range = "Name!A1:F400"
    
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range)
    
        service.executeQuery(query, delegate: self, didFinish: #selector(forGettingNames(ticket:finishedWithObject:error:)))

    }
    
    func cargoMatch() {
        
        avgCargo.text = "Getting sheet data..."
        
        let range = "\(String(sheetName.text))!C19"
        
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range)
        
        service.executeQuery(query, delegate: self, didFinish: #selector(forCargo(ticket:finishedWithObject:error:)))
        
    }
    
    func getClimb() {
        
        climb.text = "Getting sheet data..."
        
        let range = "\(String(sheetName.text))!C20"
        
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range)
        
        service.executeQuery(query, delegate: self, didFinish: #selector(forClimb(ticket:finishedWithObject:error:)))
        
    }
    
    func getInitialScout() {
        
        defense.text = "Getting sheet data..."
        
        let range = "\(String(sheetName.text))!B23"
        
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range)
        
        service.executeQuery(query, delegate: self, didFinish: #selector(forDefense(ticket:finishedWithObject:error:)))
        
        hatchesView.text = "Getting sheet data..."

        let range2 = "\(String(sheetName.text))!B25"

        let query2 = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range2)

        service.executeQuery(query2, delegate: self, didFinish: #selector(forHatches(ticket:finishedWithObject:error:)))
        
        cargo.text = "Getting sheet data..."
        
        let range3 = "\(String(sheetName.text))!B26"
        
        let query3 = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range3)
        
        service.executeQuery(query3, delegate: self, didFinish: #selector(forInitialScoutCargo(ticket:finishedWithObject:error:)))
        
        climbing.text = "Getting sheet data..."
        
        let range4 = "\(String(sheetName.text))!B27"
        
        let query4 = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range4)
        
        service.executeQuery(query4, delegate: self, didFinish: #selector(forInitialScoutClimb(ticket:finishedWithObject:error:)))
        
        specialties.text = "Getting sheet data..."
        
        let range5 = "\(String(sheetName.text))!B28"
        
        let query5 = GTLRSheetsQuery_SpreadsheetsValuesGet.query(withSpreadsheetId: spreadsheetId, range: range5)
        
        service.executeQuery(query5, delegate: self, didFinish: #selector(forInitialScoutSpecialty(ticket:finishedWithObject:error:)))

    }
    
}

