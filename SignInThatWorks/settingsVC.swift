//
//  settingsVC.swift
//  SignInThatWorks
//
//  Created by Chase Conaway on 5/29/19.
//  Copyright © 2019 Chase Conaway. All rights reserved.
//

import Foundation
import GoogleAPIClientForREST
import iOSDropDown
import GoogleSignIn
import UIKit
public var globalSpreadsheetID: String = ""


class settingsVC: UIViewController, UITextViewDelegate, UIGestureRecognizerDelegate {
    let spreadsheetID = UITextView()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        spreadsheetID.frame = CGRect(x: 0, y: 0, width: 300, height: 50)
        spreadsheetID.isEditable = true
        spreadsheetID.textAlignment = .center
        spreadsheetID.contentInset = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
        spreadsheetID.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        //defense.center = CGPoint(x: 160, y: 474)
        spreadsheetID.textColor = UIColor.white
        spreadsheetID.backgroundColor = UIColor(red: 112/255.0, green: 26/255.0, blue: 24/255.0, alpha: 1.0)
        spreadsheetID.isHidden = false
        spreadsheetID.text = "Spreadhsheet ID here"
        spreadsheetID.font = UIFont.systemFont(ofSize: 20.0)
        
        view.addSubview(spreadsheetID)
    }
    
    override func viewWillLayoutSubviews() {
        spreadsheetID.translatesAutoresizingMaskIntoConstraints = false
        spreadsheetID.topAnchor.constraint(equalTo: view.topAnchor, constant: 150).isActive = true
        spreadsheetID.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -696).isActive = true
        spreadsheetID.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        spreadsheetID.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -98).isActive = true
        
    }
    
    func setSpreadsheetID() {
        globalSpreadsheetID = String(spreadsheetID.text)
    }
    
    @IBAction func setSpreadSheetIDButton(_ sender: Any) {
        setSpreadsheetID()
    }
    
}
